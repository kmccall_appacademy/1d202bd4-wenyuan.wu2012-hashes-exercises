#starting from 921pm
# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_lengths_hash = Hash.new
  str.split.each do |el|
    word_lengths_hash[el] = el.length
  end
  word_lengths_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  sorted_arr = hash.sort_by {|k,v| v}
  sorted_arr.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k,v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  frequency_hash = Hash.new(0)
  word.each_char {|char| frequency_hash[char] = word.count(char)}
  frequency_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  arr_hash = Hash.new
  arr.each do |el|
    arr_hash[el] = arr.count(el)
  end
  arr_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  evens_and_odds_hash = Hash.new(0)
  numbers.each do |el|
    if el.odd?
      evens_and_odds_hash[:odd] += 1
    else
      evens_and_odds_hash[:even] += 1
    end
  end
  evens_and_odds_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel = 'aeiou'
  vowel_frequency = Hash.new(0)
  string.each_char do |char|
    if vowel.include?(char.downcase)
      vowel_frequency[char] += 1
    end
  end
  vowel_ranking = vowel_frequency.sort_by {|k,v| v}
  most_common = vowel_ranking[-1][1]
  if most_common != vowel_ranking[-2][1]
    return vowel_ranking.last[0]
  else
    vowel_alphabetial_ranking = vowel_frequency.sort_by {|k,v| k}
    vowel_alphabetial_ranking.each do |el|
      if el[1] == most_common
        return el[0]
      end
    end
  end
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  fall_and_winter_birthdays = students.select {|k,v| v>6}
  fall_and_winter_birthdays.keys.combination(2).to_a
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  specimens_hash = Hash.new(0)
  specimens.each do |el|
    specimens_hash[el] = specimens.count(el)
  end
  number_of_species = specimens_hash.length
  smallest_population_size = specimens_hash.sort_by {|k,v| v}.first[1]
  largest_population_size = specimens_hash.sort_by {|k,v| v}.last[1]
  number_of_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign_hash = character_count(normal_sign)
  vandalized_sign_hash = character_count(vandalized_sign)
  vandalized_sign_hash.each do |k,v|
    if !normal_sign_hash.key?(k)|| v > normal_sign_hash[k]
      return false
    end
  end
  true
end

def character_count(str)
  character_count_hash = Hash.new(0)
  str.delete(" ").downcase.each_char do |char|
    character_count_hash[char] = str.count(char)
  end
  character_count_hash
end
